# go vet

This is kept in sync with the ../go/ package.  While go already includes go vet,
the overall go package is quite huge, so we want to split the single tool out
for easier standalone use.
