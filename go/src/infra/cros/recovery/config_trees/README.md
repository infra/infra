# Config trees

Trees require to show how the final execution tree of configuration will look
like. This way we can see if we add some action where it will be applied and if
that can be executed or not as part of one of the tasks.

Trees are good to track all final representations as configs become more
complicated and more dynamic.
