# PARIS - Recovery Library

[go/fleet-recovery-developer](http://go/fleet-recovery-developer)

TL;DR - after making changes in this directory run

```
make trees
make test
```

And upload your change to gerrit with

```
git push origin HEAD:refs/for/main
```