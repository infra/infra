// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package metrics

//go:generate mockgen -copyright_file copyright.txt -source metrics.go -destination mockmetrics/metrics.mock.go -package mockmetrics
