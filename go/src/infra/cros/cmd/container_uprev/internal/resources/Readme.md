# Resources

Files embedded into the binary that can be loaded into the container's
build environment.

Note: Try to limit resources to lightweight resources for the binary's
size. Very large resources should be loaded during execution.