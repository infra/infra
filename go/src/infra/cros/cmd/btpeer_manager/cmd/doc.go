// Copyright 2023 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package cmd defines the CLI for btpeer_manager.
//
// The btpeer_manager CLI relies on the Cobra framework. See the
// Cobra documentation for more details on how to configure a Cobra CLI.
package cmd
