## Android Build API client library

Auto-generated this client library by running the following command from the current directory.
```
./gen.sh
```
The `gen.sh` script uses [google-api-go-generator](https://github.com/googleapis/google-api-go-client/tree/main/google-api-go-generator) to generate api client with a discovery json. This script also copies internal/gensupport package into our codebase as a dependency of this client library.