
## Unit Testing

### Generate mocks

To generate mocks, use the mockgen tool
```sh
$ mockgen -source=invocations.go -destination=mocks/invocations.go
```