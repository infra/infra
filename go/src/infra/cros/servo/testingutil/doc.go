// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package testingutil is a kitchen sink of utilities shared by Tast tests and
// Tast framework.
//
// TODO(crbug.com/1019099): Reorganize utilities.
package testingutil
