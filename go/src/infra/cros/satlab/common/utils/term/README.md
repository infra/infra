The expect module was copied from https://github.com/google/goterm at 555d40f
as the project was archived and not maintained and there was no maintained
alternative. 

It was required by the expect module which was also copied to the utils.

Changes made to the original code: formatted the code with gofmt, fixed typos.
