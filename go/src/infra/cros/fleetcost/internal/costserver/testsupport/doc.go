// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package testsupport provides a text fixture that sets up unit tests for the fleet cost server.
package testsupport
