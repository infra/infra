// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package maskutils is a collection of utilities for getting, setting,
// validating and doing other stuff to protos like fleetcostpb.CostIndicator
// given a fieldmask. A fieldmask can be either a fieldmask proto or a list of
// strings.
package maskutils
