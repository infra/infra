// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package controller

// Export definitions to the tests that are not used in non-tests
type IndicatorAttribute = indicatorAttribute

// Export functions as variables
var NormalizeToHourlyCost = normalizeToHourlyCost
var GetCostIndicatorValue = getCostIndicatorValue
var GetIndicatorFallbacks = getIndicatorFallbacks
var NewIndicatorAttribute = newIndicatorAttribute
var CalculateCostForSingleChromeosDut = calculateCostForSingleChromeosDut
