// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package fleetcostpb is the generated protos for the fleet cost service.
package fleetcostpb
