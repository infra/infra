// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package ufs

//go:generate mockgen -copyright_file copyright.txt -source ufs.go -destination mockufs/ufs.mock.go -package mockufs
