// Copyright 2022 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//go:generate go install go.chromium.org/luci/tools/cmd/assets
//go:generate assets -ext *.yaml
package assets
