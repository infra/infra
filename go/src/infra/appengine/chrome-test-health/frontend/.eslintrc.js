// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// eslint-disable-next-line no-undef
module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
  },
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'prettier',
    'google',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/recommended',
    'plugin:import/typescript',
    'plugin:jsx-a11y/recommended',
  ],
  'settings': {
    'react': {
      'version': 'detect',
    },
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      'typescript': {},
    },
  },
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
    },
    'ecmaVersion': 'latest',
    'sourceType': 'module',
  },
  'plugins': ['react', '@typescript-eslint', 'prettier', 'jsx-a11y', 'import'],
  'rules': {
    'max-len': 0,

    // Errors
    'object-curly-spacing': ['error', 'always', { 'objectsInObjects': true }],
    'require-jsdoc': 0,
    'import/order': ['error', {
      'pathGroups': [
        {
          'pattern': '@/**',
          'group': 'external',
          'position': 'after',
        },
      ],
    }],
    'no-trailing-spaces': 'error',
    'eol-last': ['error', 'always'],

    // Warnings
    'no-console': ['warn', { allow: ['error'] }],
    'quotes': ['warn', 'single'],
    'semi': ['warn', 'always'],
    'indent': 'warn',
    '@typescript-eslint/no-unused-vars':
      ['warn', { 'argsIgnorePattern': '^_' }],

    // Off
    'react/jsx-uses-react': 'off',
    'react/react-in-jsx-scope': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
  },
};
