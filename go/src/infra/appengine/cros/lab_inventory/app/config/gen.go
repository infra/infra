// Copyright 2019 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//go:generate cproto

// Package config contains service configuration endpoints and data definition
// for lab_inventory.
package config
