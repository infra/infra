// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package sompb

//go:generate cproto
//go:generate svcdec -type AlertsServer
