// Copyright 2019 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package enumeration contains functions to enumerate tests and associated
// metadata matching test plan requirements.
package enumeration
