// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package fleetconsolerpc is the generated protos for the fleet console service.
package fleetconsolerpc

//go:generate cproto -use-grpc-plugin
