// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package device contains library functions that help manipulate UFS device
// data for different Fleet services.
package device
