// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package devicelabel contains the implementation for generating scheduling labels
// or human-readable labels for fleet devices.
package devicelabel
