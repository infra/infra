// Copyright 2022 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.36.5
// 	protoc        v5.29.3
// source: go.chromium.org/infra/unifiedfleet/api/v1/models/chromeos/lab/rpm.proto

package ufspb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
	unsafe "unsafe"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Type of RPM that controls the device
// Next Tag: 4
type OSRPM_Type int32

const (
	OSRPM_TYPE_UNKNOWN OSRPM_Type = 0
	// Sentry Switched PDU
	OSRPM_TYPE_SENTRY OSRPM_Type = 1
	// IP Power 9850
	OSRPM_TYPE_IP9850 OSRPM_Type = 2
	// CPI PDU
	OSRPM_TYPE_CPI OSRPM_Type = 3
)

// Enum value maps for OSRPM_Type.
var (
	OSRPM_Type_name = map[int32]string{
		0: "TYPE_UNKNOWN",
		1: "TYPE_SENTRY",
		2: "TYPE_IP9850",
		3: "TYPE_CPI",
	}
	OSRPM_Type_value = map[string]int32{
		"TYPE_UNKNOWN": 0,
		"TYPE_SENTRY":  1,
		"TYPE_IP9850":  2,
		"TYPE_CPI":     3,
	}
)

func (x OSRPM_Type) Enum() *OSRPM_Type {
	p := new(OSRPM_Type)
	*p = x
	return p
}

func (x OSRPM_Type) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (OSRPM_Type) Descriptor() protoreflect.EnumDescriptor {
	return file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_enumTypes[0].Descriptor()
}

func (OSRPM_Type) Type() protoreflect.EnumType {
	return &file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_enumTypes[0]
}

func (x OSRPM_Type) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use OSRPM_Type.Descriptor instead.
func (OSRPM_Type) EnumDescriptor() ([]byte, []int) {
	return file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescGZIP(), []int{0, 0}
}

// Remote power management info.
// Next Tag: 4
type OSRPM struct {
	state           protoimpl.MessageState `protogen:"open.v1"`
	PowerunitName   string                 `protobuf:"bytes,1,opt,name=powerunit_name,json=powerunitName,proto3" json:"powerunit_name,omitempty"`
	PowerunitOutlet string                 `protobuf:"bytes,2,opt,name=powerunit_outlet,json=powerunitOutlet,proto3" json:"powerunit_outlet,omitempty"`
	PowerunitType   OSRPM_Type             `protobuf:"varint,3,opt,name=powerunit_type,json=powerunitType,proto3,enum=unifiedfleet.api.v1.models.chromeos.lab.OSRPM_Type" json:"powerunit_type,omitempty"`
	unknownFields   protoimpl.UnknownFields
	sizeCache       protoimpl.SizeCache
}

func (x *OSRPM) Reset() {
	*x = OSRPM{}
	mi := &file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_msgTypes[0]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *OSRPM) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*OSRPM) ProtoMessage() {}

func (x *OSRPM) ProtoReflect() protoreflect.Message {
	mi := &file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_msgTypes[0]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use OSRPM.ProtoReflect.Descriptor instead.
func (*OSRPM) Descriptor() ([]byte, []int) {
	return file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescGZIP(), []int{0}
}

func (x *OSRPM) GetPowerunitName() string {
	if x != nil {
		return x.PowerunitName
	}
	return ""
}

func (x *OSRPM) GetPowerunitOutlet() string {
	if x != nil {
		return x.PowerunitOutlet
	}
	return ""
}

func (x *OSRPM) GetPowerunitType() OSRPM_Type {
	if x != nil {
		return x.PowerunitType
	}
	return OSRPM_TYPE_UNKNOWN
}

var File_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto protoreflect.FileDescriptor

var file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDesc = string([]byte{
	0x0a, 0x47, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72,
	0x67, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66,
	0x6c, 0x65, 0x65, 0x74, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x73, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2f, 0x6c, 0x61, 0x62, 0x2f,
	0x72, 0x70, 0x6d, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x27, 0x75, 0x6e, 0x69, 0x66, 0x69,
	0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x6d,
	0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2e, 0x6c,
	0x61, 0x62, 0x22, 0xff, 0x01, 0x0a, 0x05, 0x4f, 0x53, 0x52, 0x50, 0x4d, 0x12, 0x25, 0x0a, 0x0e,
	0x70, 0x6f, 0x77, 0x65, 0x72, 0x75, 0x6e, 0x69, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x75, 0x6e, 0x69, 0x74, 0x4e,
	0x61, 0x6d, 0x65, 0x12, 0x29, 0x0a, 0x10, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x75, 0x6e, 0x69, 0x74,
	0x5f, 0x6f, 0x75, 0x74, 0x6c, 0x65, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0f, 0x70,
	0x6f, 0x77, 0x65, 0x72, 0x75, 0x6e, 0x69, 0x74, 0x4f, 0x75, 0x74, 0x6c, 0x65, 0x74, 0x12, 0x5a,
	0x0a, 0x0e, 0x70, 0x6f, 0x77, 0x65, 0x72, 0x75, 0x6e, 0x69, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x33, 0x2e, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64,
	0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x6d, 0x6f, 0x64,
	0x65, 0x6c, 0x73, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2e, 0x6c, 0x61, 0x62,
	0x2e, 0x4f, 0x53, 0x52, 0x50, 0x4d, 0x2e, 0x54, 0x79, 0x70, 0x65, 0x52, 0x0d, 0x70, 0x6f, 0x77,
	0x65, 0x72, 0x75, 0x6e, 0x69, 0x74, 0x54, 0x79, 0x70, 0x65, 0x22, 0x48, 0x0a, 0x04, 0x54, 0x79,
	0x70, 0x65, 0x12, 0x10, 0x0a, 0x0c, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x55, 0x4e, 0x4b, 0x4e, 0x4f,
	0x57, 0x4e, 0x10, 0x00, 0x12, 0x0f, 0x0a, 0x0b, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x53, 0x45, 0x4e,
	0x54, 0x52, 0x59, 0x10, 0x01, 0x12, 0x0f, 0x0a, 0x0b, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x49, 0x50,
	0x39, 0x38, 0x35, 0x30, 0x10, 0x02, 0x12, 0x0c, 0x0a, 0x08, 0x54, 0x59, 0x50, 0x45, 0x5f, 0x43,
	0x50, 0x49, 0x10, 0x03, 0x42, 0x45, 0x5a, 0x43, 0x67, 0x6f, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d,
	0x69, 0x75, 0x6d, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x75, 0x6e,
	0x69, 0x66, 0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76,
	0x31, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f,
	0x73, 0x2f, 0x6c, 0x61, 0x62, 0x3b, 0x75, 0x66, 0x73, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
})

var (
	file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescOnce sync.Once
	file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescData []byte
)

func file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescGZIP() []byte {
	file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescOnce.Do(func() {
		file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescData = protoimpl.X.CompressGZIP(unsafe.Slice(unsafe.StringData(file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDesc), len(file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDesc)))
	})
	return file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDescData
}

var file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_goTypes = []any{
	(OSRPM_Type)(0), // 0: unifiedfleet.api.v1.models.chromeos.lab.OSRPM.Type
	(*OSRPM)(nil),   // 1: unifiedfleet.api.v1.models.chromeos.lab.OSRPM
}
var file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_depIdxs = []int32{
	0, // 0: unifiedfleet.api.v1.models.chromeos.lab.OSRPM.powerunit_type:type_name -> unifiedfleet.api.v1.models.chromeos.lab.OSRPM.Type
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_init() }
func file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_init() {
	if File_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: unsafe.Slice(unsafe.StringData(file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDesc), len(file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_rawDesc)),
			NumEnums:      1,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_goTypes,
		DependencyIndexes: file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_depIdxs,
		EnumInfos:         file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_enumTypes,
		MessageInfos:      file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_msgTypes,
	}.Build()
	File_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto = out.File
	file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_goTypes = nil
	file_go_chromium_org_infra_unifiedfleet_api_v1_models_chromeos_lab_rpm_proto_depIdxs = nil
}
