// Copyright 2019 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package unifiedfleet.api.v1.models.chromeos.lab;

option go_package = "go.chromium.org/infra/unifiedfleet/api/v1/models/chromeos/lab;ufspb";

import "google/protobuf/timestamp.proto";
import "google/api/field_behavior.proto";
import "go.chromium.org/infra/unifiedfleet/api/v1/models/chromeos/lab/chromeos_device_id.proto";

// This proto defines status labels in lab config of a DUT.
// Next Tag: 36
message DutState {
  ChromeOSDeviceID id = 1;
  PeripheralState servo = 2;
  PeripheralState chameleon = 3;
  PeripheralState audio_loopback_dongle = 4;
  // wifi_peripheral_state is used for swarming scheduling. It represent the
  // state for wifi peripheral devices.
  PeripheralState wifi_peripheral_state = 16;
  // Indicate how many working bluetooth btpeers there are in the testbed.
  int32 working_bluetooth_btpeer = 5;
  // Combined state of all btpeers in the testbed. Only set as WORKING if all
  // btpeers are WORKING as well.
  PeripheralState peripheral_btpeer_state = 23;
  // State for the human motion robot system.
  PeripheralState hmr_state = 20;
  // State for audio latency toolkit
  PeripheralState audio_latency_toolkit_state = 22;

  // CR50-related configs by definition shouldn't be a state config, but a build
  // config. However, we don't have a way to source it from any external
  // configuration system, and it's changed frequently enough to handle cr50
  // tests, which makes it basically impossible for manual updatings: See
  // crbug.com/1057145 for the troubles it causes.
  //
  // So we temporarily set it in state config so that repair job can update it.
  // For further changes of it, please see tracking bug crbug.com/1057719.
  //
  // phases for cr50 module. Next Tag: 3
  enum CR50Phase {
    CR50_PHASE_INVALID = 0;
    CR50_PHASE_PREPVT = 1;
    CR50_PHASE_PVT = 2;
  }

  CR50Phase cr50_phase = 6;

  // key env for cr50 RW version. Next Tag: 3
  enum CR50KeyEnv {
    CR50_KEYENV_INVALID = 0;
    CR50_KEYENV_PROD = 1;
    CR50_KEYENV_DEV = 2;
  }
  // Detected based on the cr50 RW version that the DUT is running on.
  CR50KeyEnv cr50_key_env = 7;

  // Detected during running admin_audit task.
  HardwareState storage_state = 8;
  HardwareState servo_usb_state = 9;
  HardwareState battery_state = 13;
  // wifi_state represent state for DUT's internal wifi component.
  HardwareState wifi_state = 14;
  HardwareState bluetooth_state = 15;
  HardwareState cellular_modem_state = 17;
  PeripheralState starfish_state = 29;
  PeripheralState rpm_state = 12;

  // Record the last update timestamp of this MachineLSE (In UTC timezone)
  google.protobuf.Timestamp update_time = 10
      [(google.api.field_behavior) = OUTPUT_ONLY];

  // Record the hostname at that time for further analysis.
  string hostname = 11;
  // Explain why the DUT state was set.
  // The value may not be available, and is used to indicate reason of a bad
  // state.
  string dut_state_reason = 18;

  enum RepairRequest {
    REPAIR_REQUEST_UNKNOWN = 0;
    // Request to re-provision DUT.
    REPAIR_REQUEST_PROVISION = 1;
    // Request to reimage DUT by USB-key.
    REPAIR_REQUEST_REIMAGE_BY_USBKEY = 2;
    // Request to re-download image to USB-key.
    REPAIR_REQUEST_UPDATE_USBKEY_IMAGE = 3;
    // Request to re-flash firmware of the DUT.
    REPAIR_REQUEST_REFLASH_FW = 4;
  }
  // List of repair-requestes specified by external services for AutoRepair.
  repeated RepairRequest repair_requests = 19;

  // Realm the underlying machine of the DutState belongs to. Shouldn't be set by the user.
  string realm = 21 [
    (google.api.field_behavior) = OUTPUT_ONLY
  ];

  // Common version information for a device.
  VersionInfo version_info = 24;

  // State for Dolos device.
  PeripheralState dolos_state = 25;

  // FW targets name for EC/AP used for FW flash.
  string fw_ec_target = 26;
  string fw_ap_target = 27;

  string gpu_id = 28;

  // State for AMT management.
  PeripheralState amt_manager_state = 30;

  // The type of Audio Beamforming on the DUT.
  string audio_beamforming = 31;

  // State for camera on the DUT.
  HardwareState camera_state = 32;

  // The type of Fingerprint board on the DUT.
  string fingerprint_board = 34;

  // The type of Fingerprint MCU on the DUT.
  string fingerprint_mcu = 33;

  // The type of Fingerprint sensor on the DUT.
  string fingerprint_sensor = 35;
}

// Next Tag: 35
enum PeripheralState {
  // Please keep for all unknown states.
  UNKNOWN = 0;
  // Device and software on it is working as expected.
  WORKING = 1;
  // Configuration for device is not provided.
  MISSING_CONFIG = 5;
  // Configuration contains incorrect information.
  WRONG_CONFIG = 4;
  // Device is not connected/plugged.
  NOT_CONNECTED = 2;
  // Device is not reachable over ssh.
  NO_SSH = 6;
  // Device is broken or not working as expected. the state used if no specified
  // state for the issue.
  BROKEN = 3;
  // Device cannot be repaired or required manual attention to fix/replace it.
  NEED_REPLACEMENT = 7;

  // Servo specific states.
  // cr50 console missing or unresponsive.
  CR50_CONSOLE_MISSING = 13;
  // Servod daemon cannot start on servo-host because cr50 testlab not enabled.
  CCD_TESTLAB_ISSUE = 8;
  // Servod daemon cannot start on servo-host.
  SERVOD_ISSUE = 9;
  // device lid is not open.
  LID_OPEN_FAILED = 10;
  // the ribbon cable between servo and DUT is broken or not connected.
  BAD_RIBBON_CABLE = 11;
  // the EC on the DUT has issue.
  EC_BROKEN = 12;
  // Servo is not connected to the DUT.
  DUT_NOT_CONNECTED = 14;
  // Some component in servo-topology missed or not detected.
  TOPOLOGY_ISSUE = 15;
  // SBU voltage issues effect CR50 detection.
  SBU_LOW_VOLTAGE = 16;
  // CR50 SBU voltage detected but device was not enumerated.
  CR50_NOT_ENUMERATED = 17;
  // Servo serial mismatch, when servo not detected and another serial detected
  // on previous used port.
  SERVO_SERIAL_MISMATCH = 18;
  // Issue to connect to servod by XMLRPC proxy.
  SERVOD_PROXY_ISSUE = 19;
  // Issue related to servo-host. Timeout to start servod or issue with
  // detecting devices.
  SERVO_HOST_ISSUE = 20;
  // Issue related to servo_updater.
  SERVO_UPDATER_ISSUE = 21;
  // Issue detected in servod and reported by dut_controller_missing_fault
  // control.
  SERVOD_DUT_CONTROLLER_MISSING = 22;
  // Issue related to cold reset pin on the DUT.
  COLD_RESET_PIN_ISSUE = 23;
  // Issue related to warm reset pin on the DUT.
  WARM_RESET_PIN_ISSUE = 24;
  // Issue related to power button pin on the DUT.
  POWER_BUTTON_PIN_ISSUE = 25;
  // Peripheral state is not applicable.
  NOT_APPLICABLE = 26;
  // The servo device connected to debug header is not detected.
  // Reported by dut_controller_missing_fault control.
  DEBUG_HEADER_SERVO_MISSING = 27;

  // Dolos related states.
  // See platform/dolos/tools/doloscmd/proto/doloscmd.proto for the
  // definative list.

  // No power from charger detected.
  DOLOS_NO_POWER_SUPPLIED = 28;
  // No power is being delivered to the DUT.
  DOLOS_OUTPUT_POWER_FAILED = 29;
  // Internal battery management state fail.
  DOLOS_BMS_STATE_INVALID = 30;
  // SMBus failure.
  DOLOS_SMBUS_COMM_NOT_DETECTED = 31;
  // Unable to communicate with the eeprom on the battery cable.
  DOLOS_EEPROM_FAILURE = 32;
  // UART device is present but unable to communicate with it.
  DOLOS_NO_COMMUNICATION = 33;
  // UART device is missing from the host.
  DOLOS_NOT_PRESENT = 34;
}


// The states are using for DUT storage and USB-drive on servo.
// Next Tag: 5
enum HardwareState {
  // keep for all unknown state by default.
  HARDWARE_UNKNOWN = 0;
  // Hardware is in good shape and pass all verifiers.
  HARDWARE_NORMAL = 1;
  // Hardware is still good but some not critical verifiers did not pass or
  // provided border values. (used for DUT storage when usage reached 98%)
  HARDWARE_ACCEPTABLE = 2;
  // Hardware is broken or bad (did not pass verifiers).
  HARDWARE_NEED_REPLACEMENT = 3;
  // Hardware is not detected to run verifiers.
  // (used for USB-drive when it expected but not detected on the device)
  HARDWARE_NOT_DETECTED = 4;
}

// This proto define common version info we want to record from a device.
// Next Tag: 5
message VersionInfo {
  string os = 1;
  string ro_firmware = 2;
  string rw_firmware = 3;
  enum OsType {
    UNKNOWN = 0;
    CHROMEOS = 1;
    ANDROID = 2;
  }
  OsType os_type = 4;
}
