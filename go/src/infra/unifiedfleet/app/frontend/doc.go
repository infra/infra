// Copyright 2025 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package frontend implements the RPC endpoints for the inventory service
package frontend
