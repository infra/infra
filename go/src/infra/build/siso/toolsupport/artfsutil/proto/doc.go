// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package proto provides protocol buffer message for artfs.
// artfs proto can be found at sso://gbt/artfs api/ directory.
package proto
